## [1.4.0](https://gitlab.com/javallone/meeting-cost/compare/v1.3.0...v1.4.0) (2020-02-16)


### Features

* **frontend:** adding ability to join meeting using saved rate and to edit saved rate ([d8b2906](https://gitlab.com/javallone/meeting-cost/commit/d8b29069f348c2092d67d7998d32d278f131d342))
* **frontend:** adding ability to save per-second rate ([1d0f494](https://gitlab.com/javallone/meeting-cost/commit/1d0f494f2c8fe8041ff45089a558a38f32ccbcbe))


### Bug Fixes

* **frontend:** adding alt text for icons ([e936034](https://gitlab.com/javallone/meeting-cost/commit/e93603468f6af0947b851d49b24201b108fec0dd))
* **frontend:** adjusting copy for saved rate ([1c3b392](https://gitlab.com/javallone/meeting-cost/commit/1c3b3926a4b29cb16b5a973d8baf93e224adb946))
* **frontend:** adjusting spacing ([d8a98de](https://gitlab.com/javallone/meeting-cost/commit/d8a98de1a5b2032793381f06e9a738b14842f12b))
* **frontend:** localizing image alt attributes ([fa70766](https://gitlab.com/javallone/meeting-cost/commit/fa707665f6b1ce155525cfa02ae897ef6f97eb15))
* **server:** ensuring data is updated only when necessary ([8767352](https://gitlab.com/javallone/meeting-cost/commit/876735231ee041e5005394af481bf4a859027440))

## [1.3.0](https://gitlab.com/javallone/meeting-cost/compare/v1.2.1...v1.3.0) (2020-02-15)


### Features

* **frontend:** make logo a link out of a meeting ([035b664](https://gitlab.com/javallone/meeting-cost/commit/035b664598ca3ee9aeb4f5fff9359fe48f483964))


### Bug Fixes

* **frontend:** adding border-radius to interface boxes ([05c5743](https://gitlab.com/javallone/meeting-cost/commit/05c5743b3bcd47fe1e0599612d46e74970f258eb))
* **frontend:** adjusting name edit styles to minimize content movement ([852cb4f](https://gitlab.com/javallone/meeting-cost/commit/852cb4f5eed31fafbddb1cf26e9b211d803d02cc))
* **frontend:** collapsing meeting name on small screens ([996677e](https://gitlab.com/javallone/meeting-cost/commit/996677e4f0eb80e7e3a265314f1102f208928ee9))
* **frontend:** fixing call to leave meeting by *actually* passing the ID ([b5cd6e2](https://gitlab.com/javallone/meeting-cost/commit/b5cd6e2b52d9fc6f4e37a5bea40bd6329bacf36c))
* **frontend:** fixing interaction between meeting name styles and edit placeholder ([b09e336](https://gitlab.com/javallone/meeting-cost/commit/b09e33614632dfc31ac5ba25b3dfd5a9a96d0751))
* **frontend:** fixing size of start meeting form ([26ed539](https://gitlab.com/javallone/meeting-cost/commit/26ed53992bf7ab2d8cf8599a0fe2f5a502c4c491))
* **frontend:** fixing width of interface ([d1c50da](https://gitlab.com/javallone/meeting-cost/commit/d1c50daf62b76ffa3c74e05f4fcc15a0d034c5a9))
* **frontend:** improving styling of edit button ([41fe71e](https://gitlab.com/javallone/meeting-cost/commit/41fe71ed63ad4e148077e7c1abf5b72d04cc78af))
* **frontend:** preventing display of negative time or cost ([59bcaef](https://gitlab.com/javallone/meeting-cost/commit/59bcaef2d7a955ad5f0c911db6e4ee1ce82fb2a5)), closes [#6](https://gitlab.com/javallone/meeting-cost/issues/6)


### Performance Improvements

* **frontend:** splitting meeting status and link ([ae89da1](https://gitlab.com/javallone/meeting-cost/commit/ae89da10cffb962bb5a005c865e9ee328a33f118))

### [1.2.1](https://gitlab.com/javallone/meeting-cost/compare/v1.2.0...v1.2.1) (2020-02-13)


### Bug Fixes

* **frontend:** fixing alignment of confirm/cancel icons ([7dcc117](https://gitlab.com/javallone/meeting-cost/commit/7dcc11798d86a9f234af6bf5bed741d0d604467a))
* **frontend:** fixing JoinMeeting mobile styles ([8578750](https://gitlab.com/javallone/meeting-cost/commit/85787504cd93198b4126d96ca5441c814e025160))
* **frontend:** growing edit name field with value ([4e2a4ad](https://gitlab.com/javallone/meeting-cost/commit/4e2a4adaa584bf498b1ce7f96dd73a3e8ff697b5))
* **frontend:** preventing line-wrap in button labels ([09816ea](https://gitlab.com/javallone/meeting-cost/commit/09816ea896d40adb9d361c9e98a574c42ebc3f22))
* **frontend:** tweaking footer styles ([4fc3aeb](https://gitlab.com/javallone/meeting-cost/commit/4fc3aeb644e36534d968afd1a4b3684107e3efd3))
* **frontend:** tweaking spacing around meeting name ([33c31fe](https://gitlab.com/javallone/meeting-cost/commit/33c31fe25823b5f2da0dc9bb833d495519d21de5))

## [1.2.0](https://gitlab.com/javallone/meeting-cost/compare/v1.1.5...v1.2.0) (2020-02-11)


### Features

* **frontend:** adding method to meeting connector to rename meeting ([11c6ca6](https://gitlab.com/javallone/meeting-cost/commit/11c6ca6bc915df24cfe06b424ee8379acc5a37d8))
* **frontend:** adding support for renaming a meeting ([d00063a](https://gitlab.com/javallone/meeting-cost/commit/d00063aa649f86361f1ca5b289cc8a433b39fbf9))
* **server:** adding message to rename a meeting ([931d900](https://gitlab.com/javallone/meeting-cost/commit/931d900177018e53ea3c7e5611b4cfc16bcfa637))
* adding "expires in" message to the join link ([1e5d4d5](https://gitlab.com/javallone/meeting-cost/commit/1e5d4d5acadd10dd13a875e27f036f9b5bd5b09f))
* adding "expires" value to data from server ([9f14c82](https://gitlab.com/javallone/meeting-cost/commit/9f14c82271e15f6ec6d595dcf8786cd4d35fae2d))
* adding leave method to server ([df81f74](https://gitlab.com/javallone/meeting-cost/commit/df81f7415882fd2e026f457a3045cc546a7f68cb))
* leaving meeting when it expires ([bd75199](https://gitlab.com/javallone/meeting-cost/commit/bd75199722f0ad2813b36d0bc66bfc97d660fcdb))


### Bug Fixes

* adjusting styling of meeting elapsed time ([45e00ef](https://gitlab.com/javallone/meeting-cost/commit/45e00ef6b960c1e8924aadfc1c3357d99b8bf383))
* handling socket reconnections ([cae1e71](https://gitlab.com/javallone/meeting-cost/commit/cae1e71ac985173605bfbdd290a44bd68cb16ed6)), closes [#3](https://gitlab.com/javallone/meeting-cost/issues/3)

### [1.1.5](https://gitlab.com/javallone/meeting-cost/compare/v1.1.4...v1.1.5) (2020-02-08)


### Bug Fixes

* **frontend:** fixing join link URL ([9de4603](https://gitlab.com/javallone/meeting-cost/commit/9de46035b67755de72647feb6332bc85164e9d67))

### [1.1.4](https://gitlab.com/javallone/meeting-cost/compare/v1.1.3...v1.1.4) (2020-02-08)

### [1.1.3](https://gitlab.com/javallone/meeting-cost/compare/v1.1.2...v1.1.3) (2020-02-08)


### Bug Fixes

* **frontend:** navigating to meeting link when starting a meeting ([b812cc7](https://gitlab.com/javallone/meeting-cost/commit/b812cc7488824a6b31f4511b93a727a98230bc80)), closes [#2](https://gitlab.com/javallone/meeting-cost/issues/2)

### [1.1.2](https://gitlab.com/javallone/meeting-cost/compare/v1.1.1...v1.1.2) (2020-02-06)


### Bug Fixes

* fixing alignment of footer elements ([df8d3bd](https://gitlab.com/javallone/meeting-cost/commit/df8d3bd65b00e19c302059c88a3494b6e09451b8))
* fixing display of commit and version in local development ([a20e689](https://gitlab.com/javallone/meeting-cost/commit/a20e689e3f004a892c9e02d8dcea7784e76d7b83))
* set VERSION correctly during image build ([92210d4](https://gitlab.com/javallone/meeting-cost/commit/92210d46385896caef7380dd4505c74fe5edfa32))

### [1.1.1](https://gitlab.com/javallone/meeting-cost/compare/v1.1.0...v1.1.1) (2020-02-05)


### Bug Fixes

* **frontend:** fixing display of version and commit sha ([8207aa3](https://gitlab.com/javallone/meeting-cost/commit/8207aa36ad4beb36dbd50a91c5d5b0101ad827df))

## [1.1.0](https://gitlab.com/javallone/meeting-cost/compare/v1.0.0...v1.1.0) (2020-02-05)


### Features

* **frontend:** adding release version to footer ([7df65f7](https://gitlab.com/javallone/meeting-cost/commit/7df65f7dac79b08d583cdb4b6e7baccc31eae82a))

## 1.0.0 (2020-02-04)

