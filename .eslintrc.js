module.exports = {
  'env': {
    'es6': true,
    'browser': false,
    'node': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:jest/recommended'
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
    'shallow': true,
    'render': true,
    'mount': true
  },
  'parserOptions': {
    'ecmaVersion': 2020,
    'ecamFeatures': {
      'jsx': true
    },
    'sourceType': 'module'
  },
  'plugins': [
    'jest'
  ],
  'rules': {
    'arrow-parens': [
      'error',
      'as-needed'
    ],
    'arrow-spacing': [
      'error',
      {
        'before': true,
        'after': true
      }
    ],
    'comma-dangle': [
      'error',
      {
        'objects': 'never',
        'arrays': 'never',
        'imports': 'never',
        'exports': 'never',
        'functions': 'never'
      }
    ],
    'indent': [
      'error',
      2,
      {
        'SwitchCase': 1
      }
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'max-len': [
      'warn',
      {
        'code': 80
      }
    ],
    'no-unused-vars': [
      'error',
      {
        'varsIgnorePattern': '^Fragment$'
      }
    ],
    'no-var': 'error',
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'space-before-function-paren': [
      'error',
      {
        'named': 'never',
        'anonymous': 'never',
        'asyncArrow': 'always'
      }
    ],
    'template-curly-spacing': [
      'error',
      'always'
    ]
  }
};
