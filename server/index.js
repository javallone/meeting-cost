const Meeting = require('./meeting');

const createServer = ({ io, redis }) => {
  const sendMeetingStatus = async query => {
    try {
      const meeting = await Meeting.get({ redis, ...query });
      const socket = io.to(meeting.redisId);

      socket.emit('status', meeting);
    } catch (error) {
      console.log(`Error fetching meeting: ${ query }:`);
      console.error(error);
    }
  };

  io.on('connection', socket => {
    socket.on('disconnecting', () => {
      Object.keys(socket.rooms)
        .filter(room => room.startsWith('meeting_'))
        .forEach(async room => {
          try {
            const meeting = await Meeting.get({ redis, redisId: room });

            meeting.setAttendeeRate({ id: socket.id, rate: 0 });

            await meeting.save({ redis });
          } catch (error) {
            console.log(`Error removing ${ socket.id } from ${ room }:`);
            console.error(error);
          }

          sendMeetingStatus({ redisId: room });
        });
    });

    socket.on('start', async (name, callback) => {
      const meeting = new Meeting();
      meeting.name = name;

      try {
        await meeting.save({ redis });

        callback(null, meeting.id);
      } catch (error) {
        console.log('Error creating meeting:');
        console.error(error);
        callback('Failed to create meeting', null);
      }
    });

    socket.on('attend', async (id, rate, callback) => {
      if (isNaN(rate) || rate < 0) {
        callback('Invalid rate provided', {});
        return;
      }

      try {
        const meeting = await Meeting.get({ redis, id });

        if (rate !== null) {
          meeting.setAttendeeRate({ id: socket.id, rate });
          await meeting.save({ redis });
          sendMeetingStatus({ id: meeting.id });
        }

        socket.join(meeting.redisId);

        callback(null, { meeting, rate: meeting.getAttendeeRate(socket.id) });
      } catch (error) {
        console.log(`Error attending meeting ${ id }:`);
        console.error(error);
        callback('Failed to attend meeting', {});
      }
    });

    socket.on('rejoin', async (id, oldSocketId, callback) => {
      try {
        const meeting = await Meeting.get({ redis, id });

        meeting.remapAttendee({ oldId: oldSocketId, newId: socket.id });

        await meeting.save({ redis });

        socket.join(meeting.redisId);

        callback(null, meeting);
      } catch (error) {
        console.log(`Error rejoining meeting ${ id }:`);
        console.error(error);
        callback('Failed to rejoin meeting');
      }
    });

    socket.on('leave', async (id, callback) => {
      try {
        const meeting = await Meeting.get({ redis, id });

        meeting.setAttendeeRate({ id: socket.id, rate: 0 });

        await meeting.save({ redis });

        socket.leave(meeting.redisId);

        sendMeetingStatus({ id: meeting.id });
        callback(null);
      } catch (error) {
        console.log(`Error leaving meeting ${ id }:`);
        console.error(error);
        callback('Failed to leave meeting');
      }
    });

    socket.on('rename', async (id, name, callback) => {
      try {
        const meeting = await Meeting.get({ redis, id });

        meeting.name = name;

        await meeting.save({ redis });

        sendMeetingStatus({ id: meeting.id });

        callback(null);
      } catch (error) {
        console.log(`Error renaming meeting ${ id }:`);
        console.error(error);
        callback('Failed to rename meeting');
      }
    });
  });
};

module.exports = {
  createServer
};
