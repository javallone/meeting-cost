const nanoid = require('nanoid');

const EXPIRE_TIME = 300;

class Meeting {
  constructor() {
    this._ops = [];
    this._expires = -1;
    this._data = {
      id: null,
      rate: 0,
      meetingStart: Date.now(),
      checkpoint: {
        value: 0,
        time: Date.now()
      },
      attendees: {}
    };
  }

  toJSON() {
    return {
      id: this.id,
      name: this.name,
      rate: this.rate,
      meetingStart: this.meetingStart,
      checkpoint: this.checkpoint,
      expires: this.expires
    };
  }

  get id() {
    return this._data.id;
  }

  get redisId() {
    return `meeting_${ this.id }`;
  }

  get name() {
    return this._data.name;
  }

  get rate() {
    return this._data.rate;
  }

  get meetingStart() {
    return this._data.meetingStart;
  }

  get checkpoint() {
    return { ...this._data.checkpoint };
  }

  get attendees() {
    return Object.keys(this._data.attendees);
  }

  get expires() {
    return this._expires;
  }

  set name(name) {
    this._data.name = name;
    this._ops.push([
      'set',
      'name',
      JSON.stringify(this._data.name)
    ]);
  }

  updateCheckpoint() {
    const rate = this._data.rate;
    const { value, time } = this._data.checkpoint;

    this._data.checkpoint = {
      value: value + rate * (Date.now() - time) / 1000,
      time: Date.now()
    };
    this._ops.push([
      'set',
      'checkpoint',
      JSON.stringify(this._data.checkpoint)
    ]);
  }

  getAttendeeRate(id) {
    return this._data.attendees[id] || 0;
  }

  setAttendeeRate({ id, rate }) {
    const rateDelta = rate - this.getAttendeeRate(id);

    if (rateDelta !== 0) {
      this.updateCheckpoint();

      this._data.rate += rateDelta;
      this._ops.push([
        'numincrby',
        'rate',
        rateDelta
      ]);
    }

    if (rate) {
      this._data.attendees[id] = rate;
      this._ops.push([
        'set',
        `attendees[${ JSON.stringify(id) }]`,
        rate
      ]);
    } else if (this._data.attendees[id]) {
      delete this._data.attendees[id];
      this._ops.push([
        'del',
        `attendees[${ JSON.stringify(id) }]`
      ]);
    }
  }

  remapAttendee({ oldId, newId }) {
    if (!this._data.attendees[oldId]) {
      return;
    }

    this._data.attendees[newId] = this._data.attendees[oldId];
    this._ops.push([
      'set',
      `attendees[${ JSON.stringify(newId) }]`,
      this._data.attendees[oldId]
    ]);

    delete this._data.attendees[oldId];
    this._ops.push([
      'del',
      `attendees[${ JSON.stringify(oldId) }]`
    ]);
  }

  async save({ redis }) {
    if (!this._data.id) {
      this._data.id = nanoid();
      this._ops = [
        ['set', '.', JSON.stringify(this._data)]
      ];
    }

    if (this._ops.length === 0) {
      return;
    }

    const redisId = `meeting_${ this._data.id }`;
    const multi = redis.multi();

    this._ops.forEach(([op, path, value]) =>
      multi[`json_${ op }`](redisId, path, value));
    this._ops = [];

    if (this.attendees.length === 0) {
      this._expires = Date.now() + EXPIRE_TIME * 1000;
      multi.expire(redisId, EXPIRE_TIME);
    } else {
      this._expires = -1;
      multi.persist(redisId);
    }

    return new Promise((resolve, reject) => {
      multi.exec(error => {
        if (error) {
          return reject(error);
        }

        resolve();
      });
    });
  }
}

Meeting.get = async ({ redis, id, redisId }) => {
  if (!redisId) {
    redisId = `meeting_${ id }`;
  }

  return new Promise((resolve, reject) => {
    const batch = redis.batch();

    batch.json_get(redisId, '.');
    batch.pttl(redisId);

    batch.exec((error, [json, ttl]) => {
      if (error) {
        return reject(error);
      }

      if (!json) {
        reject(new Error(`Meeting not found: ${ redisId }`));
      }

      const meeting = new Meeting();
      meeting._data = JSON.parse(json);
      meeting._expires = ttl === -1 ? -1 : Date.now() + ttl;

      resolve(meeting);
    });
  });
};

module.exports = Meeting;
