module.exports = {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: 'coverage',
  coverageReporters: [
    'html',
    'text-summary'
  ],
  moduleNameMapper: {
    '\\.css$': 'identity-obj-proxy',
    '\\.svg$': '<rootDir>/__mocks__/svg-mock.js',
    '^react$': 'preact/compat',
    '^react-dom/test-utils$': 'preact/test-utils',
    '^react-dom$': 'preact/compat'
  },
  setupFiles: [
    '<rootDir>/__setup__/enzyme.js',
    '<rootDir>/__setup__/i18n.js',
    '<rootDir>/__setup__/test-helpers.js'
  ],
  snapshotSerializers: [
    'enzyme-to-json/serializer'
  ],
  timers: 'fake'
};
