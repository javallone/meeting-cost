import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

i18n
  .use(initReactI18next)
  .init({
    initImmediate: true,
    lng: 'en',
    debug: false,
    interpolation: {
      escapeValue: false
    },
    resources: {}
  });

i18n.t = (key, { lng, lngs, ns, ...options }) => {
  if (lng) {
    options.lng = lng;
  }
  if (lngs !== null) {
    options.lngs = lngs;
  }
  if (ns !== 'translation') {
    options.ns = ns;
  }

  if (Object.keys(options).length > 0) {
    return `t(${ key }, ${ JSON.stringify(options) })`;
  } else {
    return `t(${ key })`;
  }
};

export default i18n;
