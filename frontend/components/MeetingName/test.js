import { h } from 'preact';

import MeetingName from '../MeetingName';
import { mockMeeting } from '../../use-meeting';

jest.mock('../../use-meeting');

describe('MeetingName', () => {
  it('renders', () => {
    jest.spyOn(Date, 'now').mockReturnValue(123456789);
    mockMeeting({
      id: '12345',
      name: 'Example Meeting'
    });

    const wrapper = shallow(
      <MeetingName />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders nothing when there is no meeting', () => {
    mockMeeting({});

    const wrapper = shallow(
      <MeetingName />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
