import { h } from 'preact';

import MeetingLink from '../MeetingLink';
import useLocation from '../../use-location';
import { mockMeeting } from '../../use-meeting';

jest.mock('../../use-location');
jest.mock('../../use-meeting');

describe('MeetingLink', () => {
  beforeEach(() => {
    useLocation.mockReturnValue({
      browser: { href: 'http://example.com/#test-meeting-url' }
    });
  });

  it('renders', () => {
    jest.spyOn(Date, 'now').mockReturnValue(123456789);
    mockMeeting({
      id: '12345',
      expires: Date.now() + 54321
    });

    const wrapper = shallow(
      <MeetingLink />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders nothing when there is no meeting', () => {
    mockMeeting({});

    const wrapper = shallow(
      <MeetingLink />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
