import { h } from 'preact';

import StartMeeting from '../StartMeeting';
import useLocation from '../../use-location';
import { start, mockMeeting } from '../../use-meeting';

jest.mock('../../use-location');
jest.mock('../../use-meeting');

describe('StartMeeting', () => {
  let historyPush;

  beforeEach(() => {
    historyPush = jest.fn();
    useLocation.mockReturnValue({
      history: { push: historyPush }
    });
  });

  it('renders', () => {
    const wrapper = shallow(
      <StartMeeting />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders nothing when there is a meeting', () => {
    mockMeeting({
      id: '12345'
    });

    const wrapper = shallow(
      <StartMeeting />
    );
    expect(wrapper).toMatchSnapshot();
  });

  describe('starting a meeting', () => {
    it('calls preventDefault', () => {
      const mockEvent = {
        preventDefault: jest.fn()
      };

      const wrapper = mount(
        <StartMeeting />
      );

      wrapper.find('form').simulate('submit', mockEvent);

      expect(mockEvent.preventDefault).toHaveBeenCalled();
    });

    it('does not start a meeting if the name is blank', () => {
      const wrapper = mount(
        <StartMeeting />
      );

      wrapper.find('input').instance().value = '';
      wrapper.find('form').simulate('submit');

      expect(start).not.toHaveBeenCalled();
    });

    it('starts the meeting', () => {
      const wrapper = mount(
        <StartMeeting />
      );

      wrapper.find('input').instance().value = 'Test Meeting';
      wrapper.find('form').simulate('submit');

      expect(start).toHaveBeenCalledWith('Test Meeting');
    });

    it('does not watch the meeting when starting fails', async () => {
      jest.spyOn(console, 'error').mockImplementation(() => {});
      start.mockRejectedValue('test error');

      const wrapper = mount(
        <StartMeeting />
      );

      wrapper.find('input').instance().value = 'Test Meeting';
      wrapper.find('form').simulate('submit');

      await flushPromises();

      expect(historyPush).not.toHaveBeenCalled();
      expect(console.error).toHaveBeenCalledWith('test error');
    });

    it('watches the meeting once it has been started', async () => {
      start.mockResolvedValue('12345');

      const wrapper = mount(
        <StartMeeting />
      );

      wrapper.find('input').instance().value = 'Test Meeting';
      wrapper.find('form').simulate('submit');

      await flushPromises();

      expect(historyPush).toHaveBeenCalledWith('12345');
    });
  });
});
