import { h } from 'preact';

import EditMeetingName from '../EditMeetingName';

jest.mock('../../use-meeting');

describe('EditMeetingName', () => {
  it('renders', () => {
    const wrapper = shallow(
      <EditMeetingName />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
