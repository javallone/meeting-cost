import { h } from 'preact';

import JoinMeeting from '../JoinMeeting';
import { mockMeeting, attend } from '../../use-meeting';

jest.mock('../../use-meeting');

const setValue = (wrapper, selector, value) => {
  wrapper.find(selector).instance().value = value;
  wrapper.find(selector).simulate('input');
};

describe('JoinMeeting', () => {
  beforeEach(() => {
    mockMeeting({
      id: '12345',
      attending: false
    });
  });

  it('renders nothing when not watching a meeting', () => {
    mockMeeting({ id: null });
    const wrapper = shallow(
      <JoinMeeting />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders nothing when attending a meeting', () => {
    mockMeeting({ id: '12345', attending: true });
    const wrapper = shallow(
      <JoinMeeting />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders when watching but not attending a meeting', () => {
    const wrapper = shallow(
      <JoinMeeting />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('joins the meeting when the form is submitted', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    };

    const wrapper = mount(
      <JoinMeeting />
    );

    setValue(wrapper, 'input#salary', 3600000);
    setValue(wrapper, 'input#vacation', 2);
    setValue(wrapper, 'input#hours', 20);
    wrapper.find('form').simulate('submit', mockEvent);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(attend).toHaveBeenCalledWith({ id: '12345', rate: 1.0 });
  });

  it('logs an error when join fails', async () => {
    jest.spyOn(console, 'error').mockImplementation(() => {});
    attend.mockRejectedValue('example failure');

    const wrapper = mount(
      <JoinMeeting />
    );

    setValue(wrapper, 'input#salary', 3600000);
    setValue(wrapper, 'input#vacation', 2);
    setValue(wrapper, 'input#hours', 20);
    wrapper.find('form').simulate('submit');

    await flushPromises();

    expect(console.error).toHaveBeenCalledWith('example failure');
  });

  it('defaults the hours per week to 40', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    };

    const wrapper = mount(
      <JoinMeeting />
    );

    setValue(wrapper, 'input#salary', 3600000);
    setValue(wrapper, 'input#vacation', 2);
    wrapper.find('form').simulate('submit', mockEvent);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(attend).toHaveBeenCalledWith({ id: '12345', rate: 0.5 });
  });

  it('does not join when the rate is zero', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    };

    const wrapper = mount(
      <JoinMeeting />
    );

    wrapper.find('input#salary').instance().value = 0;
    wrapper.find('input#vacation').instance().value = 2;
    wrapper.find('input#hours').instance().value = 20;
    wrapper.find('form').simulate('submit', mockEvent);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(attend).not.toHaveBeenCalled();
  });

  it('does not join when the rate is negative', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    };

    const wrapper = mount(
      <JoinMeeting />
    );

    wrapper.find('input#salary').instance().value = -1;
    wrapper.find('input#vacation').instance().value = 2;
    wrapper.find('input#hours').instance().value = 20;
    wrapper.find('form').simulate('submit', mockEvent);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(attend).not.toHaveBeenCalled();
  });

  it('does not join when the rate is NaN', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    };

    const wrapper = mount(
      <JoinMeeting />
    );

    wrapper.find('input#salary').instance().value = 0;
    wrapper.find('input#vacation').instance().value = 0;
    wrapper.find('input#hours').instance().value = 0;
    wrapper.find('form').simulate('submit', mockEvent);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(attend).not.toHaveBeenCalled();
  });

  it('does not join when the rate is Infinite', () => {
    const mockEvent = {
      preventDefault: jest.fn()
    };

    const wrapper = mount(
      <JoinMeeting />
    );

    wrapper.find('input#salary').instance().value = 3600000;
    wrapper.find('input#vacation').instance().value = 2;
    wrapper.find('input#hours').instance().value = 0;
    wrapper.find('form').simulate('submit', mockEvent);

    expect(mockEvent.preventDefault).toHaveBeenCalled();
    expect(attend).not.toHaveBeenCalled();
  });
});
