const useLocation = jest.fn();

beforeEach(() => {
  useLocation.mockReset();
  useLocation.mockReturnValue({
    browser: {},
    history: {},
    location: {}
  });
});

export default useLocation;
