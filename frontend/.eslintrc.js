module.exports = {
  'env': {
    'es6': true,
    'browser': true,
    'node': false
  },
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:jest/recommended'
  ],
  'globals': {
    'process': 'readonly',
    'flushPromises': 'readonly'
  },
  'plugins': [
    'react',
    'jest'
  ],
  'rules': {
    'react/no-unknown-property': 'off'
  },
  'settings': {
    'react': {
      'version': 'latest',
      'pragma': 'h'
    }
  }
};
