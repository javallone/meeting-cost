FROM node:12-alpine

# Set PROD to "true" for production build
ARG PROD=""

ARG VERSION
ENV VERSION ${VERSION:-##NO_VER##}

ARG COMMIT
ENV COMMIT ${COMMIT:-##NO_COMMIT##}

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn install

COPY . .
RUN yarn build
RUN \
      if [ "$PROD" == "true" ]; then \
        rm -r node_modules .cache && \
        yarn install --prod; \
      fi

# Using multi-stage build to keep image size down
FROM node:12-alpine

WORKDIR /usr/src/app

COPY --from=0 /usr/src/app .

HEALTHCHECK \
  CMD wget -O /dev/null http://localhost:8080/?healthcheck || exit 1

CMD [ "yarn", "start" ]
