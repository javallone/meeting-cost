const express = require('express');
const morgan = require('morgan');
const redisAdapter = require('socket.io-redis');
const redis = require('redis');
const rejson = require('redis-rejson');
const { createServer } = require('./server');

const port = process.env.PORT || 8080;
const hmrPort = process.env.HMR_PORT || 8081;
const isProd = (process.env.NODE_ENV === 'production');

rejson(redis);

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT
});

const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

io.adapter(redisAdapter({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT
}));

createServer({ io, redis: redisClient });

app.use(morgan('dev'));

(async () => {
  if (isProd) {
    console.log('Starting as production app');
    app.use(express.static('./dist'));
  } else {
    console.log('Starting as development app');
    const Bundler = require('parcel-bundler');

    await new Promise((resolve, reject) => {
      const bundler = new Bundler('./frontend/index.html', {
        hmrPort,
        autoInstall: false
      });

      bundler.on('bundled', resolve);
      bundler.on('buildError', reject);

      app.use(bundler.middleware());
    });
  }
})().then(
  () => http.listen(port, () => console.log(`Listening on port ${ port }`)),
  error => console.error(error)
);
