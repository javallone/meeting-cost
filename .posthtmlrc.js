const favicons = require('posthtml-favicons');
const expressions = require('posthtml-expressions');
const inlineSvg = require('posthtml-inline-svg');

const package = require('./package.json');

module.exports = {
  plugins: [
    favicons({
      root: './frontend',
      configuration: {
        appName: 'Meeting Cost',
        appDescription: package.description,
        developerName: 'Jeff Avallone',
        developerUrl: 'https://gitlab.com/javallone',
        lang: 'en-US',
        version: package.versiona,
        icons: {
          favicons: true,
          android: false,
          appleIcon: false,
          appleStartup: false,
          coast: false,
          firefox: false,
          windows: false,
          yandex: false,
        }
      }
    }),
    expressions({
      locals: {
        ...process.env
      }
    }),
    inlineSvg({
      cwd: './frontend'
    })
  ]
};
